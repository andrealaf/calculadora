<%-- 
    Document   : result
    Created on : Apr 10, 2021, 3:46:23 PM
    Author     : Andrea
--%>

<%@page import="model.Calculador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
  Calculador cal=(Calculador)request.getAttribute("calculador");
 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <h1>Resultado</h1>
            <form name="form" action="CalculadoraController" method="POST">  
        <div class="form-group">
            <label for="codigo">Interés simple generado: <%= cal.calcular()%></label>         
        </div>
            
            </form>
    </body>
</html>
