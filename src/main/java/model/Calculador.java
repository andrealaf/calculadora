/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Andrea
 */
public class Calculador {
     /**
     * @return the calculo
     */
    public int getCalculo() {
        return calculo;
    }
    
    private int capital;
    private int tasa;
    private int plazo;
    private int calculo;
    
    /**
     * @param calculo the calculo to set
     */
    public void setCalculo(int calculo) {
        this.calculo = calculo;
    }

    /**
     * @return the capital
     */
    public int getCapital() {
        return capital;
    }


    /**
     * @param capital the capital to set
     */
    public void setCapital(int capital) {
        this.capital = capital;
    }

    /**
     * @return the tasa
     */
    public int getTasa() {
        return tasa;
    }

    /**
     * @param tasa the tasa to set
     */
    public void setTasa(int tasa) {
        this.tasa = tasa;
    }
    
      /**
     * @return the plazo
     */
    public int getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public int calcular() {
       
        //I = C * (i/100) * n
        return this.getCapital() * this.getTasa()/100 * this.getPlazo();

    }

    public int calcular(int capital, int tasa, int plazo) {
        
        this.setCapital(capital);
        this.setTasa(tasa);
        this.setPlazo(plazo);

        return calcular();

    }
}
